Ejercicio - ASP
====================

Proyecto para capacitación en tecnología ASP

- - -

## BMW Control de incidencias
Aplicación móvil para control de incidencias en obras Civiles de BMW, con el principal objetivo de reducir el tiempo de reacción.

- - -

## Prerrequisitos

Para preparar tu ambiente de desarrollo se requiere tener instalado:

[Pendiente]
 
[Lista de Plugins utilizada, cada una con su versión específica]

- - -

## Configuración de ambiente de desarrollo

[Pendiente]

- - -

## Despliegue

[Pendiente]

- - -

## Versionamento

Utilizamos [SemVer](https://semver.org/ ) para el versionamiento del proyecto.

- - -

## Autor

Inflexion 
http://inflexionsoftware.com 

![Inflexion](http://inflexionsoftware.com/images/logo-inflexion-1.png)
