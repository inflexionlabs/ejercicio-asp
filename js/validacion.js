
    function soloNumeros(evt) {
        if (window.event) {//asignamos el valor de la tecla a keynum
            keynum = evt.keyCode; //IE
        }
        else {
            keynum = evt.which; //FF
        }
        //comprobamos si se encuentra en el rango numérico y que teclas no recibirá.
        if ((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13 || keynum == 6) {
            return true;
        }
        else {
            return false;
        }
    }
function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toString();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere que se muestre.
    especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}

function addRefered(aux) {
    var auxRefered = true;
    var name = $("#idNameRefered").val();
    var phone = $("#idPhoneRefered").val();
    var email = $("#idEmailRefered").val();
    var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);

        
    if ( name == "") {
        $("#idNameRefered").addClass("input-error");
        auxRefered = false;
    }
    if (phone == "") {
        $("#idPhoneRefered").addClass("input-error");
        auxRefered = false;
    }
    if (caract.test(email) == false) {
        $("#idEmailRefered").addClass("input-error");
        return true;
    }
    if (email == "") {
        $("#idEmailRefered  ").addClass("input-error");
        auxRefered = false;
    }

    if (auxRefered != false) {
        $("#idNewRefered").show();
        $("#idNewRefered").append('<div id="newRefered'+aux+'" style="margin-left:5%;"><tr>'+
            '<td>Nombre: <input id="idNameNewRefered'+aux+'" readOnly style="border:none; width:30%;" value ="' + name + '"/></td></tr>' +
            '<td>Teléfono: <input id="idPhoneNewRefered' + aux + '" readOnly style="border:none; width:18%;" value ="' + phone + '"/></td></tr>' +
            '<td>Email: <input id="idEmailNewRefered' + aux + '" readOnly style="border:none; width:20%;" value ="' + email + '"/></td></tr>' +
            '<td><input id="idDeleteRefered' + aux + '" type="button" value="Eliminar" onclick="deleteRefered(newRefered'+aux+'")"/></td></tr>' +
            '</div>');
    }
}

function deleteRefered(divDelete) {
    $(divDelete).remove();
}

function savePFP() {
    if (!validateForm()) {
        return;
    }
}

function validateForm() {
    $("#idErrorMessage").empty();
    var auxValidate = true;
    var activity = $("#idActivity").val();
    var dependents = $("#idDependents").val();
    var auxPriorities = false;
    var auxPriorities2 = false;
    var cont = 0;
    var cont2 = 0;

    if (activity == "") {
        $("#idActivity").addClass("input-error");
        $("#idErrorMessage").append("<li>Ingrese la actividad principal</li>");
        auxValidate = false;
    }
    if (dependents == "") {
        $("#idDependents").addClass("input-error");
        $("#idErrorMessage").append("<li>Asigne un número de dependientes, en caso de no tener ingrese un cero</li>");
        auxValidate = false;
    }
    if ((dependents > 0) && (dependents <9)){
        $("#idErrorMessage").append("<li>Asigne las edades de los dependientes.</li>");
        auxValidate = false;
    }
    //para saber cuantas checks estan seleccionados
    for (i = 1; i < 18; i++) {
        if (i < 13) {
            if ($("#idNoFinancialNeeds" + i).is(':checked')) {
                cont = cont + 1;                    
            }
        }
        if ($("#idNoFinancialNeeds" + i).is(':checked')) {
            cont2 = cont2 + 1;
        }
    }
    //valida que las prioridades cuenten con un valor si la necesidad está activa
    for (i = 1; i < 18; i++) {
        if (i < 13) {
            if ($("#idNoFinancialNeeds" + i).is(':checked')) {
                if ($("#idPriority" + i).val() == "") {
                    $("#idPriority" + i).addClass("input-error");
                    auxPriorities = true;
                }
                if ($("#idPriority" + i).val() > cont) {
                    $("#idPriority" + i).addClass("input-error");
                    auxPriorities2 = true;
                }
            }
        } else {
            if ($("#idNoFinancialNeeds" + i).is(':checked')) {
                if ($("#idPriority" + i).val() == "") {
                    $("#idPriority" + i).addClass("input-error");
                    auxPriorities = true;
                }
                if ($("#idPriority" + i).val() > cont2) {
                    $("#idPriority" + i).addClass("input-error");
                    auxPriorities2 = true;
                }
            }
        }
    }

    if (auxPriorities) {
        $("#idErrorMessage").append("<li>Asigne un valor a las prioridades, no pueden estar vacias</li>");
        auxValidate = false;
    }
    if (auxPriorities2) {
        $("#idErrorMessage").append("<li>La prioridad no puede ser mayor al número de necesidades seleccionadoas</li>");
        auxValidate = false;
    }
    if (cont < 3) {
        $("#idErrorMessage").append("<li>Elija por lo menos 3 necesidades</li>");
        auxValidate = false;
    }

    if (auxValidate == false) {
        $("#idErrorMessage").show();
        $("#idErrorMessage").prepend("<h3 style=color:red;>Por favor corrija los siguientes errores:</h3>");
        $("#idErrorMessage").append("<br/>");
    }



}


function activeDependents(input, event) {

    $("#idDependents").removeClass("input-error");
                
    $("#idDependents2").fadeOut();
    $(".ageDependents").prop("disabled", true);
    $(".ageDependents").val("");
    var dependents = $("#idDependents").val();

    if ((dependents > 4) && (dependents < 9)) {
        $("#idDependents2").fadeIn();
    }
    if ((dependents > 0) && (dependents < 9)) {
        for (i = 1; i <= dependents; i++) {
            $("#idAge" + i).prop("disabled", false);
        }
    } else {
        $("#idDependents").addClass("input-error");
        console.log("error");
    }
}

function validNull(input) {
    var aux = $(input).val();
    if (aux == "") {
        $(input).addClass("input-error");
    }
}
function removeError(input) {
    var aux = $(input).val();
    if (aux != "") {
        $(input).removeClass("input-error");
    }
}

function validRepet(input) {
    var priorityRep = $(input).val();

    for (i = 1; i < 13; i++) {
        if (priorityRep == $("#idPriority" + i).val()) {
            $(input).addClass("input-error");
        }
    }
}

function activeBuniness() {
    $("#idBuniness").show();
}
function noActiveBuniness() {
    $("#idBuniness").fadeOut();
}
   