USE [master]
GO
/****** Object:  Database [EJERCICIO_PFP]    Script Date: 27/07/2018 04:41:29 p. m. ******/
CREATE DATABASE [EJERCICIO_PFP]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'EJERCICIO_PFP', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\EJERCICIO_PFP.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'EJERCICIO_PFP_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\EJERCICIO_PFP_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [EJERCICIO_PFP] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EJERCICIO_PFP].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EJERCICIO_PFP] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET ARITHABORT OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EJERCICIO_PFP] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EJERCICIO_PFP] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EJERCICIO_PFP] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EJERCICIO_PFP] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET RECOVERY FULL 
GO
ALTER DATABASE [EJERCICIO_PFP] SET  MULTI_USER 
GO
ALTER DATABASE [EJERCICIO_PFP] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EJERCICIO_PFP] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EJERCICIO_PFP] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EJERCICIO_PFP] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [EJERCICIO_PFP] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'EJERCICIO_PFP', N'ON'
GO
ALTER DATABASE [EJERCICIO_PFP] SET QUERY_STORE = OFF
GO
USE [EJERCICIO_PFP]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [EJERCICIO_PFP]
GO
/****** Object:  Table [dbo].[analisis_financiero]    Script Date: 27/07/2018 04:41:30 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[analisis_financiero](
	[id] [bigint] NOT NULL,
	[id_usuario] [bigint] NULL,
	[beneficios_pago_nomina] [varchar](50) NULL,
	[beneficios_transferencia_saldos] [varchar](50) NULL,
	[credito_gastos] [varchar](50) NULL,
	[adquirir_casa_propia] [varchar](50) NULL,
	[adquirir_auto] [varchar](50) NULL,
	[seguro] [varchar](50) NULL,
	[financiar_ahorrar_pagos] [varchar](50) NULL,
	[consolidar_deudas] [varchar](50) NULL,
	[financiar_ahorrar_eventos] [varchar](50) NULL,
	[adquirir_nueva_casa] [varchar](50) NULL,
	[acceso_mercados_financieros] [varchar](50) NULL,
	[fondo_retiro] [varchar](50) NULL,
 CONSTRAINT [PK_analisis_financiero] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuario]    Script Date: 27/07/2018 04:41:30 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuario](
	[id] [bigint] NOT NULL,
	[nombre] [varchar](max) NULL,
	[actividad] [varchar](max) NULL,
	[cliente] [varchar](max) NULL,
	[dependientes_economicos] [int] NULL,
	[edades] [varchar](50) NULL,
	[telefono] [varchar](50) NULL,
	[correo] [varchar](50) NULL,
 CONSTRAINT [PK_usuario] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [EJERCICIO_PFP] SET  READ_WRITE 
GO
