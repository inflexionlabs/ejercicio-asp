<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>EJERCICIO PFP</title>
<link rel="stylesheet" href="estilo.css" type="text/css" media="screen" />
<!--include file="../content/save_pfp.asp"-->
<script type:"text/javascript" src="../Lib/jquery.js"></script>
<script type="text/javascript" src="../JS/validacion.js"></script>
<script type="text/javascript">
    var aux = 0;
    function soloNumeros(evt) {
        if (window.event) {//asignamos el valor de la tecla a keynum
            keynum = evt.keyCode; //IE
        }
        else {
            keynum = evt.which; //FF
        }
        //comprobamos si se encuentra en el rango numérico y que teclas no recibirá.
        if ((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13 || keynum == 6) {
            return true;
        }
        else {
            return false;
        }
    }
    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toString();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
        especiales = [8, 37, 39, 46, 6];

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }

    function addRefered() {
        var auxRefered = true;
        var name = $("#idNameRefered").val();
        var phone = $("#idPhoneRefered").val();
        var email = $("#idEmailRefered").val();
        var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);

        if ( name == "") {
            $("#idNameRefered").addClass("input-error");
            auxRefered = false;
        }
        if (phone == "") {
            $("#idPhoneRefered").addClass("input-error");
            auxRefered = false;
        }
        if (caract.test(email) == false) {
            $("#idEmailRefered").addClass("input-error");
            return true;
        }
        if (email == "") {
            $("#idEmailRefered").addClass("input-error");
            auxRefered = false;
        }
        
        if (auxRefered == true) {
            aux = aux + 1;
            $("#idNewRefered").show();
            $("#idNewRefered").append('<div id="newRefered'+aux+'" style="margin-left:5%;"><tr>'+
                '<td>Nombre'+aux+': <input id="idNameNewRefered'+aux+'" readOnly style="border:none; width:30%;" value ="' + name + '"/></td></tr>' +
                '<td>Teléfono: <input id="idPhoneNewRefered' + aux + '" readOnly style="border:none; width:18%;" value ="' + phone + '"/></td></tr>' +
                '<td>Email: <input id="idEmailNewRefered' + aux + '" readOnly style="border:none; width:20%;" value ="' + email + '"/></td></tr>' +
                '<td><input id="idDeleteRefered' + aux + '" type="button" value="Eliminar" onclick="deleteRefered(newRefered' + aux + ')"/></td></tr>' +
                '</div>');

            $("#idNameRefered").prop("value", "");
            $("#idPhoneRefered").prop("value", "");
            $("#idEmailRefered").prop("value", "");
        }

        if (aux >= 2) {
            $("#idRefereds").hide();
        }

        
        console.log("aux", aux);
    }

    function deleteRefered(div) {
        $(div).remove();
        aux = aux - 1;
        console.log(aux);
        if (aux < 2) {
            $("#idRefereds").show();
        }

    }

    function savePFP() {
        if (!validateForm()) {
            return;
        }
    }

    function validateForm() {
        $("#idErrorMessage").empty();
        var auxValidate = true;
        var activity = $("#idActivity").val();
        var dependents = $("#idDependents").val();
        var auxPriorities = false;
        var auxPriorities2 = false;
        var cont = 0;
        var cont2 = 0;

        if (activity == "") {
            $("#idActivity").addClass("input-error");
            $("#idErrorMessage").append("<li>Ingrese la actividad principal</li>");
            auxValidate = false;
        }
        if (dependents == "") {
            $("#idDependents").addClass("input-error");
            $("#idErrorMessage").append("<li>Asigne un número de dependientes, en caso de no tener ingrese un cero</li>");
            auxValidate = false;
        }
        if ((dependents > 0) && (dependents <9)){
            $("#idErrorMessage").append("<li>Asigne las edades de los dependientes.</li>");
            auxValidate = false;
        }
        //para saber cuantas checks estan seleccionados
        for (i = 1; i < 18; i++) {
            if (i < 13) {
                if ($("#idNoFinancialNeeds" + i).is(':checked')) {
                    cont = cont + 1;                    
                }
            }
            if ($("#idNoFinancialNeeds" + i).is(':checked')) {
                cont2 = cont2 + 1;
            }
        }
        //valida que las prioridades cuenten con un valor si la necesidad está activa
        for (i = 1; i < 18; i++) {
            if (i < 13) {
                if ($("#idNoFinancialNeeds" + i).is(':checked')) {
                    if ($("#idPriority" + i).val() == "") {
                        $("#idPriority" + i).addClass("input-error");
                        auxPriorities = true;
                    }
                    if ($("#idPriority" + i).val() > cont) {
                        $("#idPriority" + i).addClass("input-error");
                        auxPriorities2 = true;
                    }
                }
            } else {
                if ($("#idNoFinancialNeeds" + i).is(':checked')) {
                    if ($("#idPriority" + i).val() == "") {
                        $("#idPriority" + i).addClass("input-error");
                        auxPriorities = true;
                    }
                    if ($("#idPriority" + i).val() > cont2) {
                        $("#idPriority" + i).addClass("input-error");
                        auxPriorities2 = true;
                    }
                }
            }
        }
        if (auxPriorities) {
            $("#idErrorMessage").append("<li>Asigne un valor a las prioridades, no pueden estar vacias</li>");
            auxValidate = false;
        }
        if (auxPriorities2) {
            $("#idErrorMessage").append("<li>La prioridad no puede ser mayor al número de necesidades seleccionadoas</li>");
            auxValidate = false;
        }
        if (cont < 3) {
            $("#idErrorMessage").append("<li>Elija por lo menos 3 necesidades</li>");
            auxValidate = false;
        }
        if (auxValidate == false) {
            $("#idErrorMessage").show();
            $("#idErrorMessage").prepend("<h3 style=color:red;>Por favor corrija los siguientes errores:</h3>");
            $("#idErrorMessage").append("<br/>");
        }        
    }


    function activeDependents(input, event) {

        $("#idDependents").removeClass("input-error");
                
        $("#idDependents2").fadeOut();
        $(".ageDependents").prop("disabled", true);
        $(".ageDependents").val("");
        var dependents = $("#idDependents").val();

        if ((dependents > 4) && (dependents < 9)) {
            $("#idDependents2").fadeIn();
        }
        if ((dependents > 0) && (dependents < 9)) {
            for (i = 1; i <= dependents; i++) {
                $("#idAge" + i).prop("disabled", false);
            }
        } else {
            $("#idDependents").addClass("input-error");
            console.log("error");
        }
    }

    function validNull(input) {
        var aux = $(input).val();
        if (aux == "") {
            $(input).addClass("input-error");
        }
    }
    function removeError(input) {
        var aux = $(input).val();
        if (aux != "") {
            $(input).removeClass("input-error");
        }
    }
    //VALIDAR REPETIDOS 
    function validRepet(input) {
        var priorityRep = $(input).val();

        for (i = 1; i < 13; i++) {
            if (priorityRep == $("#idPriority" + i).val()) {
                $(input).addClass("input-error");
            }
        }
    }

    function activeBuniness() {
            $("#idBuniness").show();
    }
    function noActiveBuniness() {
        $("#idBuniness").fadeOut();
    }
   


    </script>
    <style type="text/css">
        .importDataTable {
            height: 9px;
        }
    </style>
  </head>
  <body>
    <div id="wrapper">
        <form action="#" method="post" class="form" id="idForm">
            <img src="../assest/logo_hsbc.gif" alt="Alternate Text" />
            <div class="encabezado">
                <label><b>Fecha:</b></label>
                <!--input type="date" id=""/-->
                <%response.write(date())%>
            </div>

            <div class="personaInformation">
                <table class="date">
                    
                    <tr>
                        <td><b>CIS/Nombre:</b><input class="dataName border-button" type="text" name="idName" id="idName" onblur="validNull(this); removeError(this);" onkeypress="return soloLetras(event);"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Actividad Principal:</b> <input class="actividad border-button" type="text" name="name" id="idActivity" onblur="validNull(this); removeError(this);" onkeypress="return soloLetras(event);"/><b>Cliente:</b>
                            <input type="radio" name="name" id="idPF" value="PF" checked  onclick="noActiveBuniness()"/>
                            <label>PF</label>
                            <input type="radio" name="name" id="idPFAE" value="PFAE" onclick="activeBuniness()"/>
                            <label>PFAE</label>
                            <input type="radio" name="name" id="idPM" value="PM" onclick="noActiveBuniness()"/>
                            <label>PM</label>
                        </td>               
                    </tr>
                    <tr>
                        <td>
                            <b>No. de dependientes económicos: </b><input class="datosA border-button" type="text" id="idDependents" name="idDependents" onclick="" onblur="activeDependents(this, event); removeError(this);" style="text-align:center;" maxlength="1"/><b> Edades</b>
                            <input class="ageDependents" type="text" id="idAge1" name="idAge1" value="" maxlength="2" onblur="validNull(this); removeError(this);" onKeyPress="return soloNumeros(event);" disabled/>
                            <input class="ageDependents" type="text" id="idAge2" name="idAge2" value="" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);" disabled/>
                            <input class="ageDependents" type="text" id="idAge3" name="idAge3" value="" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);" disabled/>
                            <input class="ageDependents" type="text" id="idAge4" name="idAge4" value="" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);" disabled/>
                        </td>
                    </tr>
                    <tr style="display: none;" id="idDependents2">
                        <td style="padding-left: 387px;">
                            <input class="ageDependents" type="text" id="idAge5" name="idAge5" value="" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);" disabled/>
                            <input class="ageDependents" type="text" id="idAge6" name="idAge6" value="" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);" disabled/>
                            <input class="ageDependents" type="text" id="idAge7" name="idAge7" value="" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);" disabled/>
                            <input class="ageDependents" type="text" id="idAge8" name="idAge8" value="" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);" disabled/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <label><b>Teléfono: </b></label><input class="dataName border-button" type="text" id="idPhone" name="idPhone" maxlength="10" onKeyPress="return soloNumeros(event);"/>
                        </td>
                    </tr>
                    <tr>
                        <td><label>
                        <b>Correo: </b></label>
                        <input class="dataName border-button" type="text" name="idName" id="idEmail" />
                        </td>
                    </tr>
                </table>
            </div>

            <div>
                <p>En <b>HSBC</b> te ofrecemos asesoría para ayudarte a construir paso a paso tu patrimonio, realizar tus sueños y asegurar tu bienestar</p>
                <p>Desde el punto de vista financiero existen necesidades que se consideran universales, es decir, que aplican para todas las personas. Nuestro objetivo será conocer qué es lo que usted ha alcanzado y en qué le podemos ayudar en adelante.</p>
            </div>

            <div>
                <nav class="titles">
                    <h4>Análisis Financiero (necesidades financieras universales)</h4>
                </nav>
                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th colspan="2">Cubierto</th>
                            <th rowspan="2">Comentarios</th>
                            <th class="input-priority">Prioridad</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>SI</th>
                            <th>NO</th>
                            <th class="input-priority">(1,2,3,...)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><img src="../assest/icon1.png" alt="img" class="img" /></td>
                            <td style="width: 100%;">Recibir beneficios por el pago de nómina</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds1" name="FinancialNeeds1" type="radio" value="on" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds1" name="FinancialNeeds1" type="radio" value="off" class="radio"/></td>
                            <td rowspan="7" class="borde"><input type="text"  style="HEIGHT: 300px; border:none; margin-left:2px"/></td>
                            <td class="checkbox"><input type="text" id="idPriority1" name="idPriority1" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon2.png" alt="img" class="img" /></td>
                            <td>Tener Beneficios preferenciales con la transferencia de tus saldos</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds2" name="FinancialNeeds2" type="radio" value="on" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds2" name="FinancialNeeds2" type="radio" value="off" class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority2" name="idPriority2" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon3.png" alt="img" class="img" /></td>
                            <td>Linea de crédito para emergencias o gastos diarios</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds3" name="FinancialNeeds3" type="radio" value="on" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds3" name="FinancialNeeds3" type="radio" value="off" class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority3" name="idPriority3" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon4.png" alt="img" class="img"/></td>
                            <td>Adquirir una casa propia</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds4" name="FinancialNeeds4" type="radio" value="on" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds4" name="FinancialNeeds4" type="radio" value="off" class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority4" name="idPriority4" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon5.png" alt="img" class="img"/></td>
                            <td>Adquirir un auto nuevo o seminuevo / Renovar o adquirir uno adicional</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds5" name="FinancialNeeds5" type="radio" value="on" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds5" name="FinancialNeeds5" type="radio" value="off" class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority5" name="idPriority5" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon6.png" alt="img" class="img"/></td>
                            <td>Protegerte a ti, a tu familia y patrimonio de enfermedades, accidentes, fallecimientos e imprevistos</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds6" name="FinancialNeeds6" type="radio" value="on" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds6" name="FinancialNeeds6" type="radio" value="off" class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority6" name="idPriority6" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon7.jpg" alt="img" class="img"/></td>
                            <td>Financiar/Ahorar para eventos importantes como: pago de deuda, educación, maternidad, etc.</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds7" name="FinancialNeeds7" type="radio" value="on" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds7" name="FinancialNeeds7" type="radio" value="off" class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority7" name="idPriority7" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr class="moreNeeds" style="display:none;">
                            <td></td>
                            <td><input  style="margin-left:55%;" type="button" id="moreNeeds" value="Más necesidades"/></td>  
                        </tr>
                        <tr>
                            <td><img src="../assest/icon8.png" alt="img"class="img" /></td>
                            <td>Consolidar deudas con un financiamiento a tasa de interés más baja</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds8" name="FinancialNeeds8" type="radio" value="on" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds8" name="FinancialNeeds8" type="radio" value="off" class="radio"/></td>
                            <td rowspan="3" class="borde"><input type="text" style="HEIGHT: 10px; border:none; margin-left:2px;"/></td>
                            <td class="checkbox"><input type="text" id="idPriority8" name="idPriority8" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon9.png" alt="img" class="img"/></td>
                            <td>Financiar/Ahorrar para eventos o proyectos importantes como una boda, graduación o ampliar su casa, etc.</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds9" name="FinancialNeeds9" type="radio" value="on" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds9" name="FinancialNeeds9" type="radio"  value="off"class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority9" name="idPriority9" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon4.png" alt="img" class="img"/></td>
                            <td>Poder financiar el enganche para una segunda casa</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds10" name="FinancialNeeds10" type="radio" value="on" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds10" name="FinancialNeeds10" type="radio" value="off" class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority10" name="idPriority10" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr class="moreOrless" style="display:none;">
                            <td></td>
                            <td><input style="margin-left:40%;" type="button" id="lessNeeds" value="Menos necesidades"/>
                                <input style="margin-left:10%;" type="button" id="Button1" value="Más necesidades"/>
                            </td>  
                        </tr>
                        <tr>
                            <td><img src="../assest/icon11.png" alt="img" class="img"/></td>
                            <td>Tener acceso a mercados financieros</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds11" name="FinancialNeeds11" type="radio" value="on" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds11" name="FinancialNeeds11" type="radio" value="off" class="radio"/></td>
                            <td rowspan="2" class="borde"><input type="text" style="HEIGHT: 75px; border:none; margin-left:2px;"/></td>
                            <td class="checkbox"><input type="text" id="idPriority11" name="idPriority11" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon12.png" alt="img" class="img"/></td>
                            <td>Contar o mejorar tu fondo para el retiro</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds12" name="FinancialNeeds12" type="radio" value="on" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds12" name="FinancialNeeds12" type="radio" value="off" class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority12" name="idPriority12" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr class="lessNeeds" style="display:none;">
                            <td></td>
                            <td><input  style="margin-left:55%;" type="button" id="Button2" value="Menos necesidades"/>
                            </td>  
                        </tr>
                    </tbody>
                </table>
            </div>    
            
            <div id="idBuniness" style="display:none;">
                <nav class="titles">
                    <h4>Análisis Financieros para negocio(llegar únicamente en caso de ser socio o dueño de un negocio)</h4>
                </nav>

                <table>
                   <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th colspan="2">Cubierto</th>
                            <th rowspan="2">Comentarios</th>
                            <th class="input-priority">Prioridad</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>SI</th>
                            <th>NO</th>
                            <th>(1,2,3,...)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><img src="../assest/icon13.png" alt="img" class="img"/></td>
                            <td style="width: 100%">Optimizar el pago a empleados mediante ahorro a cuentas bancarias</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds13" name="FinancialNeeds13" type="radio" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds13" name="FinancialNeeds13" type="radio" class="radio"/></td>
                            <td rowspan="6" class="borde"></td>
                            <td class="checkbox"><input type="text" id="idPriority13" name="idPriority13" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon3.png" alt="img" class="img"/></td>
                            <td>Recibir pagos en su negocio con tarjetas bancarias</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds14" name="FinancialNeeds14" type="radio" value="" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds14" name="FinancialNeeds14" type="radio" value="" class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority14" name="idPriority14" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon13.png" alt="img"class="img" /></td>
                            <td>Otorgar un seguro de vida a sus empleados</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds15" name="FinancialNeeds15" type="radio" value="" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds15" name="FinancialNeeds15" type="radio" value="" class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority15" name="idPriority15" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon14.jpg" alt="img" class="img"></td>
                            <td>Contar un seguro en caso de que falte el hombre clave de negocio</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds16" name="FinancialNeeds16" type="radio"value="" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds16" name="FinancialNeeds16" type="radio"value="" class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority16" name="idPriority16" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon15.png" alt="img" class="img"/></td>
                            <td>Administrar finanzas del Negocio y Personales con una Banca Electrónica, un contacto, un equipo especializado y cuota única</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds17" name="FinancialNeeds17" type="radio"value="" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds17" name="FinancialNeeds17" type="radio"value="" class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority17" name="idPriority17" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                        <tr>
                            <td><img src="../assest/icon16.png" alt="img" class="img" /></td>
                            <td>Financiar la ampliación de su negocio o de compra de activo fijo</td>
                            <td class="checkbox"><input id="idYessFinancialNeeds18" name="FinancialNeeds18" type="radio"value="" class="radio" checked/></td>
                            <td class="checkbox"><input id="idNoFinancialNeeds18" name="FinancialNeeds18" type="radio"value="" class="radio"/></td>
                            <td class="checkbox"><input type="text" id="idPriority18" name="idPriority18" class="priority" maxlength="2" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div>
                <nav class="titles">
                    <h4>Conociendo tus prioridades, ahora revisemos tu situación financiera para ayuda a cumplirlas</h4>
                </nav>
                <h4>Gasto mensual familiar, incluyendo el dependientes económicos:</h4>
                <table style="width:60%; margin-left:20%;">
                    <tr>
                        <td><li>Alimentación (comida, despensa, etc.)</li></td>
                        <td><input type="text" class="classExpense" name="idFeeding" id="idFeeding" onkeypress="return soloNumeros(event);"/></td>
                    </tr>
                    <tr>
                        <td><li>Educación (colegiatura, uniformes) </li></td>
                        <td><input type="text" class="classExpense" name="idEducation" id="idEducation" onkeypress="return soloNumeros(event);"/></td>
                    </tr>
                    <tr>
                        <td><li>Servicios (agua, luz, telefonía, netflix)</li></td>
                        <td><input type="text" class="classExpense" name="idServices" id="idServices" onkeypress="return soloNumeros(event);"/></td>
                    </tr>
                    <tr>
                        <td><li>Transporte (gasolina, pasajes, estacionamiento)</li></td>
                        <td><input type="text" class="classExpense" name="idTransport" id="idTransport" onkeypress="return soloNumeros(event);"/></td>
                    </tr>
                    <tr>
                        <td><li>Entretenimiento (cine,teatro, etc)</li></td>
                        <td><input type="text" class="classExpense" name="idEntertainment" id="idEntertainment" onkeypress="return soloNumeros(event);"/></td>
                    </tr>
                    <tr>
                        <td><li>Créditos(tarjeta de crédito, personal, auto)</li></td>
                        <td><input type="text" class="classExpense" name="" id="idCredit" onkeypress="return soloNumeros(event);"/></td>
                    </tr>
                    <tr>
                        <td><li>Renta de casa/hipoteca</li></td>
                        <td><input type="text" class="classExpense" name="idMorgage" id="idMorgage" onkeypress="return soloNumeros(event);"//></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input style="border:none; border-bottom:solid 3px; width:100%;" onkeypress="return soloNumeros(event);"//></td>
                    </tr>
                    <tr>
                        <td><h4>Gasto mensual familiar Total</h4></td>
                        <td><input type="text" class="classExpense" name="idFamilyExpense" id="idFamilyExpense" onblur="validNull(this); removeError(this);"/ onkeypress="return soloNumeros(event);"/></td>
                    </tr>
                    <tr>
                        <td>Ahorro acumulado</td>
                        <td><input type="text" class="classExpense" name="idAccumulatedSavings" id="idAccumulatedSavings" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"//></td>
                    </tr>

                    <tr>
                        <td>Protección o seguro para imprevistos (suma asegurada)</td>
                        <td><input type="text" class="classExpense" name="" id="id" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"//></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input style="border:none; border-bottom:solid 3px; width:100%;"/></td>
                    </tr> 
                    <tr>
                        <td></td>
                        <td><input type="text" class="classExpense" name="" id="Text1" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"//></td>
                    </tr>   
                    <tr>
                        <td><p>Gasto mensual <b>$</b><input style="width: 15%; border: none; border-bottom: solid 2px;" onkeypress="return soloNumeros(event);"//><b>(X) 24 meses</b></p></td>
                        <td><input type="text" class="classExpense" name="idMonthlyExpense" id="idMonthlyExpense" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"//></td>
                    </tr>
                    <tr>
                        <td>Protección sugerida (B-A)     =</td>
                        <td><input type="text" class="classExpense" name="idProtecton" id="idProtecton" onblur="validNull(this); removeError(this);" onkeypress="return soloNumeros(event);"//></td>
                    </tr>
                    
                </table>
            </div>

            <!--div>
                <nav class="titles">
                    <h4>Conociendo tus prioridades, ahora revisemos tu situación financiera para ayuda a cumplirlas</h4>
                </nav>
                <h4>Gasto mensual familiar, incluyendo el dependientes económicos:</h4>
                <div class="familyExpense">
                    <ul>
                        <li>Alimentación (comida, despensa, etc.)</li>
                        <li>Educación (colegiatura, uniformes)</li>
                        <li>Servicios (agua, luz, telefonía, netflix)</li>
                        <li>Transporte (gasolina, pasajes, estacionamiento)</li>
                        <li>Entretenimiento (cine,teatro, etc)</li>
                        <li>Créditos(tarjeta de crédito, personal, auto)</li>
                        <li>Renta de casa/hipoteca</li>
                   </ul>
                   <br />
                   <p><b>Gasto mensual familiar Total</b></p>

                   <p>Ahorro acumulado</p>
                   <p>Protección o seguro para imprevistos (suma asegurada)</p>
         
                   <br />
                   <ul class="line">
                       <li>Gasto mensual</li>
                       <li>$</li>
                       <li><input style="width: 15%; border: none; border-bottom: solid;" /></li>
                       <li><b>(X) 24 meses</b></li>
                   </ul>                   
                   <p><b>Protección sugerida (B-A)     =</b></p>
                  
                </div>
                <div class="spending">
                    <ul>
                        <li><input type="text" class=""/></li>
                        <li><input type="text" class=""/></li>
                        <li><input type="text" class=""/></li>
                        <li><input type="text" class=""/></li>
                        <li><input type="text" class=""/></li>
                        <li><input type="text" class=""/></li>
                        <li><input type="text" class=""/></li>
                    </ul>
                    <hr style="width: 60%; margin-right: 40%;">
                    <ul>
                        <li><input type="text"/></li>
                        <li><br /></li>
                        <li><input type="text"/></li>
                        <li><br /></li>
                        <li><input type="text"/></li>
                    </ul>
                    <hr style="width: 60%; margin-right: 40%;">
                    <ul>
                        <li><input type="text"/></li>
                        <li><input type="text"/></li>
                       <li><input type="text" style="margin-top: 10px;" /></li>
                   </ul>
                </div>
            </div-->
       
            <div><!-- class="new"-->
                <nav class="titles">
                    <h4>Recomendaciones</h4>
                </nav>
                <table style="width: 100%;">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Necesidades Diarias</th>
                            <th></th>
                            <th>Crecer el patrimonio</th>
                            <th></th>
                            <th>Proteger el patrimonio</th>
                            <th></th>
                            <th>Proyecto importante</th>
                            <th></th>
                            <th>Retiro/Legado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <input type="checkbox" id="idRoster" name="idRoster"/>
                            </td> 
                            <td>
                                <p>Portabilidad/Nómina</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idMortgage" name="idMortgage"/>
                            </td> 
                            <td>
                                <p>Hipotecario</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idLifeInsurance" name="idLifeInsurance"/>
                            </td> 
                            <td>
                                <p>Seguro de Vida/hospital</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idDotal" name="idDotal"/>
                            </td> 
                            <td>
                                <p>Dotal</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idInvestment" name="idInvestment"/>
                            </td> 
                            <td>
                                <p>Inversion retiro</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" id="idCreditCard" name="idCreditCard"/>
                            </td> 
                            <td>
                                <p>Tarjeta de Crédito</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idInvestmentFund" name="idInvestmentFund"/>
                            </td> 
                            <td>
                                <p>Fondo de inversión</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idCarInsurance" name="idCarInsurance"/>
                            </td> 
                            <td>
                                <p>Seguro de Automóvil</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idTHN" name="idTHN"/>
                            </td> 
                            <td>
                                <p>Crédito simple/THN</p>
                            </td>  
                            <td>
                                <input type="checkbox" id="idFund" name="idFund"/>
                            </td>
                            <td>
                                <p>Fondos de inversión</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" id="idCreditPerson" name="idCreditPerson"/>
                            </td> 
                            <td>
                                <p>Crédit nómina/personal</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idTerm" name="idTerm"/>
                            </td> 
                            <td>
                                <p>Plazo</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idHomeInsurance" name="idHomeInsurance"/>
                            </td> 
                            <td>
                                <p>Seguro casa/habitación</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idInsuranceHause" name="idInsuranceHause"/>
                            </td> 
                            <td>
                                <p>Fondo de inversión</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idAfore" name="idAfore"/>
                            </td> 
                            <td>
                                <p>AFORE</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" id="idCreditCar" name="idCreditCar"/>
                            </td> 
                            <td>
                                <p>Crédito auto</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idTPV" name="idTPV"/>
                            </td> 
                            <td>
                                <p>TPV</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idGroupLife" name="idGroupLife"/>
                            </td> 
                            <td>
                                <p>Vida grupo</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idFusion" name="idFusion"/>
                            </td> 
                            <td>
                                <p>Membresia Fusion</p>
                            </td>
                            <td>
                                <input type="checkbox" id="idKeyman" name="idKeyman"/>
                            </td> 
                            <td>
                                <p>Seguro Keyman</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" id="idTransfer" name="idTransfer"/>
                            </td> 
                            <td>
                                <p>Transferencia de saldos</p>
                            </td>
                            <td>
                            </td> 
                            <td>
                                <p></p>
                            </td>
                            <td>
                            </td> 
                            <td>
                                <p></p>
                            </td>
                            <td>
                            </td> 
                            <td>
                                <p></p>
                            </td>
                            <td>
                            </td> 
                            <td>
                                <p></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div>
                <nav class="titles">
                    <h4>Seguro de Vida/Ahorro seguro HSBC</h4>
                </nav>
                <table style="width: 100%;" class="">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th rowspan="2">Suma asegurada</th>
                            <th rowspan="2">Principal beneficio</th>
                            <th rowspan="2">Meses de protección(para cubrir el gasto familiar)</th>
                            <th rowspan="2">Pago Mensual</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="checkbox" id="idInsurancePerson"/></td> 
                            <td><p>Seguro de vida individual</p></td>
                            <td><input type="text" id="idProtectionMedical"/></td>
                            <td>Protección ante cáncer e infarto a un costo accesible</td>
                            <td><input style="width: 90%;" type="text" id="Text2"/></td>
                            <td><input type="text" id="Text3"/></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="idSaveSaving"/></td> 
                            <td><p>Ahorro Seguro</p></td>
                            <td><input type="text" id="idSaving"/></td>
                            <td>Ahorro = Suma asegurada</td>
                            <td><input style="width: 90%;" type="text" id="Text4"/></td>
                            <td><input type="text" id="Text5"/></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="divContainer">
                <nav class="titles">
                    <h4>Nuevo Referido</h4>
                </nav>
                <p>Contacto de algún familiar o amigo a quien podría interesarle un análisis financiero como el que acabamos de realizar:</p>
                <div id="idRefereds">       
                    <table style="width: 100%;">               
                        <tbody class="">
                            <tr>
                                <td class="classRefered">Nombre:</td>
                                <td><input type="text" id="idNameRefered" style="width:100%;" value="" onblur="removeError(this);" onkeypress="return soloLetras(event);"/></td> 
                                <td class="classRefered" >Teléfono:</td>
                                <td><input type="text" id="idPhoneRefered"  style="width:100%;" value="" onblur="removeError(this);" onkeypress="return soloNumeros(event);"/></td>
                                <td class="classRefered" >Correo:</td>
                                <td><input type="text" id="idEmailRefered" style="width:100%;" value="" onblur="removeError(this);"/></td>
                                <td class="classRefered" ><input type="button" value="Agregar" id="idAddNew" onclick="addRefered()"/></td>
                            </tr>
                            <!--tr>
                                <td>Nombre:</td>
                                <td><input type="text" id=""/></td> 
                                <td></td>
                                <td><p>Teléfono:</p></td>
                                <td><input type="text" id=""/></td>
                                <td></td>
                                <td>Correo:</td>
                                <td><input type="text" id=""/></td>
                            </tr-->
                        </tbody>
                    </table>
                </div>

                <div id="idNewRefered" style="display:none">

                </div>
            </div>

            <div id="idErrorMessage" class="ErrorMessage" style="border: 1px solid rgba(0, 0, 0, .5);">           
            </div>
            <br />

            <input type="button" id="idSavePFP" value="Agregar plan de finanzas" class="SavePFP" onclick="savePFP()"/>

            <div class="tabla" style="height:50px;">

                <h2 class="tnks">¡Muchas gracias por su atención!</h2>
            </div>

            <div class="tabla" style="height:50px;">
                <table class="datos_asesor">
                    <tr>
                        <td><b><label>Asesor Financiero:</label></b>
                            <input type="text" id="idNameProm" style="width: 62%; border:none; border-bottom:solid 1px;" onkeypress="return soloNumeros(event);"//>                            
                        </td>
                    </tr>
                    <tr>
                        <td><label><b>Teléfono:</b></label>
                            <input type="text" id="idPhoneProm" style="width: 80%; border:none; border-bottom:solid 1px;" maxlength="10;" onKeyPress="return soloNumeros(event);"/>
                        </td>               
                      </tr>
                      <tr>
                          <td><label>
                          <b>Sucursal:</b></label>
                              <input type="text" id="idSucur" style="width:79.5%;  border:none; border-bottom:solid 1px;"/>  
                          </td>
                      </tr>              
                  </table>
            </div>

            
            <div>
                <p>Este documento no implica ningún tipo de aceptación de los seguros descritos en el cuerpo del presente, ni constituye una solicitud de seguro. Los precios, productos descritos, así como sus sumas aseguradas son un estimado y están sujetos a cambio sin previo aviso. Consulta las coberturas, exclusiones, restricciones y requisitos de contratación de los seguros en www.hsbc.com.mx. Los seguros son registrados y operados por HSBC. Seguros S.A de C.V. Grupo financiero HSBC</p>
            </div>

            <div class="importData">
                <table class="importDataTable" width="100%" border="1" cellpadding="0" cellspacing="1" bordercolor="#000000"  style="border-collapse:collapse;border-color:#7c0b5a;">
                    <tr>
                        <th>No. PFP</th>
                        <th>Fecha de creación</th>
                        <th>Gasto mensual familiar</th>
                        <th>Ahorro acumulado</th>
                        <th>Suma asegurada</th>
                        <th>Gasto anual</th>
                        <th>Protección sugerida</th>
                    </tr>

                </table>

            </div>

        </form>
</div>
</body>
</html>